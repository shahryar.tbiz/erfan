defmodule Erfan do
  @moduledoc """
  Erfan keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """
  @alphabet Enum.concat([?0..?9, ?A..?Z, ?a..?z])

  def randstring(count) do
    :rand.seed(:exsplus, :os.timestamp())
    Stream.repeatedly(&random_char_from_alphabet/0)
    |> Enum.take(count)
    |> List.to_string()
    |> String.upcase
  end

  defp random_char_from_alphabet() do
    Enum.random(@alphabet)
  end

  def convert_changeset_errors(changeset) do
    out =  Ecto.Changeset.traverse_errors(changeset, fn {msg, opts} ->
          Enum.reduce(opts, msg, fn {key, value}, acc ->
            String.replace(acc, "%{#{key}}", convert_value_of_changeset_to_string(value))
          end)
      end)
    out
  end

  defp convert_value_of_changeset_to_string(value) when is_list(value) do
    List.first(value) |> to_string
  end

  defp convert_value_of_changeset_to_string(value) do
    to_string(value)
  end

end
