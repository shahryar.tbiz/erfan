defmodule ErfanWeb.Router do
  use ErfanWeb, :router

  import Phoenix.LiveView.Router
  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", ErfanWeb do
    pipe_through :browser

    live "/", Live.Index
    live "/categories", Live.Categories
    live "/categories/new", Live.Categories.New
    live "/categories/edit/:id", Live.Categories.Edit
    live "/category/foods/:id", Live.Foods
    live "/category/foods/new/:id", Live.Foods.New
    live "/category/foods/edit/:id/:category_id", Live.Foods.Edit


    live "/invoices", Live.Invoices
    live "/invoices/:page_number", Live.Invoices
    live "/invoice/new", Live.Invoices.New
    live "/invoice/show", Live.Invoices.Show


    # live "/paginate", Live.Paginate 


  end

  # Other scopes may use custom stacks.
  # scope "/api", ErfanWeb do
  #   pipe_through :api
  # end
end
