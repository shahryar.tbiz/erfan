defmodule ErfanWeb.Live.Foods.Edit do
   use Phoenix.LiveView

   alias Erfan.Admin.CategoryQuery
   alias Erfan.Admin.FoodSchema

   alias ErfanWeb.Router.Helpers, as: Routes

   def mount(%{"id" => id, "category_id" => category_id}, _session, socket) do
      {:ok, fetch(socket, id, category_id)}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("foods_new.html", assigns)
   end

   defp fetch(socket, id, category_id) do
      case load_food(id, socket) do
         {:ok, changeset, food_name} -> 
            assign(socket, changeset: changeset, page_title: "
            ویرایش غذای
            :
            #{food_name}
         ", category_id: category_id)

         {:error, :get_category_by_id} ->
               socket
               |> redirect(to: Routes.live_path(ErfanWeb.Endpoint, Categories))
      end
   end

   def load_food(id, socket) do
      case CategoryQuery.get_food_by_id(id) do
         {:ok, :get_food_by_id, food_info} -> 
            {:ok, FoodSchema.changeset(food_info), food_info.title}

            {:error, :get_food_by_id} -> {:error, :get_food_by_id}
      end
   end

   def handle_event("validate", %{"food_schema" => food_info}, socket) do

      changeset = %FoodSchema{} 
      |> FoodSchema.changeset(food_info)
      |> Map.put(:action, :update)

      {:noreply, assign(socket, changeset: changeset)}
   end

   def handle_event("save", %{"food_schema" => food_info}, socket) do
      case CategoryQuery.edit_food(food_info["id"], food_info) do
         {:ok, _resutl} ->

            Erfan.Notif.notify_subscribers(:ok, "Food has been updated", [:category, :new])

            {:noreply,
               socket
               |> push_redirect(to: Routes.live_path(socket, ErfanWeb.Live.Foods, food_info["category_id"]))
            }
         {:error, _, %Ecto.Changeset{} = changeset} ->
           {:noreply, assign(socket, changeset: changeset)}

         _ ->
            {:noreply,
               socket
               |> push_redirect(to: Routes.live_path(socket, ErfanWeb.Live.Foods, food_info["category_id"]))
            } 
       end
   end
end
