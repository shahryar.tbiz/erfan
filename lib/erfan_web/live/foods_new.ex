defmodule ErfanWeb.Live.Foods.New do
   use Phoenix.LiveView

   alias Erfan.Admin.CategoryQuery
   alias Erfan.Admin.FoodSchema

   alias ErfanWeb.Router.Helpers, as: Routes

   def mount(%{"id" => id}, _session, socket) do
      if connected?(socket), do: CategoryQuery.subscribe()
      {:ok, assign(socket, page_title: "ساخت غذا", category_id: id, changeset: %FoodSchema{} |> FoodSchema.changeset())}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("foods_new.html", assigns)
   end

   def handle_event("validate", %{"food_schema" => food_info}, socket) do

      changeset = %FoodSchema{} 
      |> FoodSchema.changeset(food_info)
      |> Map.put(:action, :insert)

      {:noreply, assign(socket, changeset: changeset)}
   end

   def handle_event("save", %{"food_schema" => category_info}, socket) do
      case CategoryQuery.create_food(category_info) do
         {:ok, _resutl} ->

            Erfan.Notif.notify_subscribers(:ok, "Food has been created", [:category, :new])

            {:noreply,
               socket
               |> push_redirect(to: Routes.live_path(socket, ErfanWeb.Live.Foods, category_info["category_id"]))
            }
         {:error, _, %Ecto.Changeset{} = changeset} ->
           {:noreply, assign(socket, changeset: changeset)}
       end
   end
end
