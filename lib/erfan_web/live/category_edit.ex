defmodule ErfanWeb.Live.Categories.Edit do
   use Phoenix.LiveView
   alias Erfan.Admin.CategoryQuery
   alias Erfan.Admin.CategorySchema 
   alias ErfanWeb.Live.Categories
   alias ErfanWeb.Router.Helpers, as: Routes

   def mount(%{"id" => id}, _session, socket) do
      if connected?(socket), do: CategoryQuery.subscribe()
      {:ok, fetch(socket, id)} 
   end

   def render(assigns) do
      ErfanWeb.PageView.render("category_new.html", assigns)
   end

   defp fetch(socket, id) do
      case load_category(id, socket) do
         {:ok, changeset, category_name} -> 
            assign(socket, changeset: changeset, page_title: "
            ویرایش مجموعه
            :
            #{category_name}
         ")

         {:error, :get_category_by_id} ->
               socket
               |> redirect(to: Routes.live_path(ErfanWeb.Endpoint, Categories))
      end
   end

   def load_category(id, socket) do
      case CategoryQuery.get_category_by_id(id) do
         {:ok, :get_category_by_id, category_info} -> 
            {:ok, CategorySchema.changeset(category_info), category_info.title}

         {:error, :get_category_by_id} ->
            {:error, :get_category_by_id}
      end
   end

   def handle_event("validate", %{"category_schema" => category_info}, socket) do

      changeset = %CategorySchema{} 
      |> CategorySchema.changeset(category_info)
      |> Map.put(:action, :update)

      {:noreply, assign(socket, changeset: changeset)}
   end

   def handle_event("save", %{"category_schema" => category_info}, socket) do
      case CategoryQuery.edit_category(category_info["id"], category_info) do
         {:ok, resutl} ->

            Erfan.Notif.notify_subscribers(:ok, "Category has been edited", [:category, :edit])
            {:noreply,
               socket
               |> push_redirect(to: Routes.live_path(socket, Categories))
            }
         {:error, _, %Ecto.Changeset{} = changeset} ->
           {:noreply, assign(socket, changeset: changeset)}
           _ ->
            {:noreply,
               socket
               |> push_redirect(to: Routes.live_path(socket, Categories))
            }  
       end
   end

   def handle_info({CategoryQuery, [:category, _], _}, socket) do
      {:noreply, socket}
   end
end