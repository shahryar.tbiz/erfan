defmodule ErfanWeb.Live.Notification do
   use Phoenix.LiveView
   alias Erfan.Notif
   def mount(_params, _session, socket) do
      Erfan.Notif.subscribe()
      {:ok, assign(socket, notifs: [])}
   end

   def render(assigns) do
      ~L"""
         <section class="container rtl notcoustom">
            <%= for notif <- @notifs do %>
               <div class="toast fade show  rtl" role="alert" aria-live="assertive" aria-atomic="true">
                  <div class="toast-header">
                  <svg class="bd-placeholder-img rounded  " width="20" height="20" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect fill="#007aff" width="100%" height="100%"></rect></svg>
                  <strong class="mr-2">پیام فوری</strong>
                  <small class="mr-auto text-muted text-left"> همین الان </small>
                  <button phx-click="close-notif" phx-value-notif-id="<%= notif.notif_id %>" type="button" class="ml-2 mb-1 close mr-3" data-dismiss="toast" aria-label="Close">
                     <span aria-hidden="true">×</span>
                  </button>
                  </div>
                  <div class="toast-body">
                  <%= notif.msg %>
                  </div>
               </div>
                  <div class="clearfix"></div>
                  <div class="spacer10">  </div>
                  <div class="clearfix"></div>
            <% end %>
         </section>
      """
   end

   def handle_event("close-notif", value, socket) do
      {:noreply, assign(socket, notifs: drop_notif(socket, value))}
   end
   
   def handle_info({Notif, event, msg, status}, socket) do
      notifs = [
         %{notif_id: Ecto.UUID.generate, event: event, msg: msg, status: status}
         | 
         socket.assigns.notifs
      ]
      {:noreply, assign(socket, notifs: notifs)}
   end

   defp drop_notif(socket, %{"notif-id" => notif_id}) do
      Enum.reject(socket.assigns.notifs, fn x -> x.notif_id == notif_id end)
   end
 end
 