defmodule ErfanWeb.Live.Invoices.Show do
   use Phoenix.LiveView
   alias Erfan.Admin.InvoiceQuery
   alias Erfan.Admin.InvoiceSchema
   alias Erfan.Admin.CategoryQuery
   alias ErfanWeb.Router.Helpers, as: Routes

   def mount(%{"invoice_id" => invoice_id}, _session, socket) do
      if connected?(socket), do: InvoiceQuery.subscribe()
      {:ok, fetch(socket, invoice_id)}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("invoices_show.html", assigns)
   end

   defp fetch(socket, invoice_id) do
      {:ok, :get_invoice_by_id, invoice} = InvoiceQuery.get_invoice_by_id(invoice_id)
      
      assign(socket, invoices: InvoiceQuery.show_invoice_by_id(invoice_id), invoice_foods: InvoiceQuery.show_invoice_foods_with_invoice_id(invoice_id, "delete"), page_title: "نمایش فاکتور", invoice_changeset: InvoiceSchema.changeset(invoice), invoice_edit: false, invoice_food_edit: false, client_foods: [], food_category: [], foods: [])
   end

   def handle_event("delete_invoice", %{"invoice-id" => invoice_id}, socket) do
      InvoiceQuery.delete_invoice_and_invoice_foods(invoice_id)
      Erfan.Notif.notify_subscribers(:ok, "Invoice has been deleted", [:invoice, :deleted])
      {:noreply,
         socket
         |> push_redirect(to: Routes.live_path(socket, ErfanWeb.Live.Invoices))
      }
   end


   def handle_event("print_invoice", %{"invoice-id" => invoice_id}, socket) do
      IO.inspect "print_invoice"
      {:noreply, socket}
   end


   def handle_event("edit_invoice", %{"invoice-id" => _invoice_id}, socket) do
      if socket.assigns.invoice_edit do
         {:noreply, assign(socket, invoice_edit: false)}
      else
         {:noreply, assign(socket, invoice_edit: true)}
      end
   end


   def handle_event("edit_invoice_food", %{"invoice-id" => invoice_id}, socket) do
      if socket.assigns.invoice_food_edit do
         {:noreply, assign(socket, invoice_food_edit: false)}
      else
         {:noreply, assign(socket, invoice_food_edit: true, client_foods: InvoiceQuery.show_invoice_foods_with_invoice_id(invoice_id, "delete"), food_category: CategoryQuery.show_categories())}
      end
   end

   def handle_event("delete-client-food-list", %{"foodid" => foodid}, socket) do
      client_foods = Enum.reject(socket.assigns.client_foods, fn x -> x.food_id == foodid end)
      {:noreply, assign(socket, client_foods: client_foods )}
   end

   
   def handle_event("select_food_category", %{"food-category-id" => category_id}, socket) do
      {:noreply, assign(socket, foods: CategoryQuery.show_category_foods(category_id) )}
   end
   

   def handle_event("save-client-food", value, socket) do
      {:ok, :get_food_by_id, food_info} = CategoryQuery.get_food_by_id(value["foodid"])
      new_client_food = %{
         title: food_info.title,
         food_id: food_info.id,
         price: food_info.price,
         number: value["number"]
      }
      merge_client_food = [new_client_food | socket.assigns.client_foods]

      {:noreply, assign(socket, client_foods: merge_client_food)}
   end

   def handle_event("validate_invoice", %{"invoice_schema" => invoice_info}, socket) do

      changeset = %InvoiceSchema{} 
      |> InvoiceSchema.changeset(invoice_info)
      |> Map.put(:action, :update)

      {:noreply, assign(socket, invoice_changeset: changeset)}
   end

   def handle_event("save_invoice", %{"invoice_schema" => invoice_info}, socket) do
      case InvoiceQuery.edit_invoice(invoice_info["id"], invoice_info) do
         {:ok, resutl} ->
            Erfan.Notif.notify_subscribers(:ok, "Invoice has been edited", [:invoice, :edit])
            {:noreply, fetch(socket, resutl.id)}

         {:error, %Ecto.Changeset{} = changeset} ->
            {:noreply, assign(socket, invoice_changeset: changeset)}

          _ ->  
            {:noreply,
               socket
               |> push_redirect(to: Routes.live_path(socket, ErfanWeb.Live.Invoices))
            }
      end
   end

   def handle_info({InvoiceQuery, [:invoice, :edit_invoice], result}, socket) do
      {:noreply, fetch(socket, result.id)}
   end

   def handle_info({InvoiceQuery, [:invoice, _], _}, socket) do
      {:noreply, socket}
   end

   def handle_event("save_updated_invoice_foods", %{"invoice-id" => invoice_id}, socket) do

      InvoiceQuery.edit_invoice(invoice_id, Map.merge(socket.assigns.invoices, %{total_amount: sum_client_list_foods_amount(socket.assigns.client_foods) + ErfanWeb.Live.Paginate.integer_geter(socket.assigns.invoices.service)}))
      InvoiceQuery.delete_invoice_foods(invoice_id)
      save_invoice_foods(invoice_id, socket.assigns.client_foods)
      Erfan.Notif.notify_subscribers(:ok, "Invoice has been updated", [:invoice, :edited])
      {:noreply, fetch(socket, invoice_id)}
   end

   defp sum_client_list_foods_amount(list_of_foods) do
      Enum.map(list_of_foods, fn x -> x.price * ErfanWeb.Live.Paginate.integer_geter(x.number) end)
      |> Enum.sum()
   end

   defp save_invoice_foods(invoice_id, client_foods) do
      Enum.map(client_foods, fn x -> 
         InvoiceQuery.create_invoice_food(%{
            invoice_id: invoice_id,
            number: x.number,
            food_id: x.food_id
         })
      end)
   end
 end
 