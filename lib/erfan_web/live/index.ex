defmodule ErfanWeb.Live.Index do
   use Phoenix.LiveView
   alias ErfanWeb.Router.Helpers, as: Routes
   alias ErfanWeb.Live.Categories
   
   def mount(_params, _session, socket) do
      {:ok, assign(socket, page_title: "داشبورد کافه")}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("index.html", assigns)
   end

   def handle_event("go_to_categories", _value, socket) do
      {:noreply, 
         push_redirect(socket, to: Routes.live_path(socket, Categories))
      }
    end
 end
 