defmodule ErfanWeb.Live.Categories do
   use Phoenix.LiveView
   alias Erfan.Admin.CategoryQuery

   def mount(_params, _session, socket) do
      if connected?(socket), do: CategoryQuery.subscribe()
      {:ok, fetch(socket)}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("categories.html", assigns)
   end

   defp fetch(socket) do
      assign(socket, categories: CategoryQuery.show_categories(), page_title: "مجموعه ها")
   end

   def handle_params(params, _uri, socket) do
      {:noreply, assign(socket, params: params)}
   end

   def handle_event("delete_category", value, socket) do
      Erfan.Notif.notify_subscribers(:ok, "Category has been deleted", [:category, :delete])
      CategoryQuery.delete_category(value["category-id"])
      {:noreply, assign(socket, notif_status: true)}
   end

   def handle_info({CategoryQuery, [:category, :create_category], _}, socket) do
      {:noreply, fetch(socket)}
   end
   
   def handle_info({CategoryQuery, [:category, :edit_category], _}, socket) do
      {:noreply, fetch(socket)}
   end

   def handle_info({CategoryQuery, [:category, :delete], _}, socket) do
      {:noreply, fetch(socket)}
   end

   def handle_info({CategoryQuery, [:category, _], _}, socket) do
      {:noreply, fetch(socket)}
   end
 end
 