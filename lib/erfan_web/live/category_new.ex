defmodule ErfanWeb.Live.Categories.New do
   use Phoenix.LiveView
   alias Erfan.Admin.CategoryQuery
   alias Erfan.Admin.CategorySchema 
   alias ErfanWeb.Live.Categories
   alias ErfanWeb.Router.Helpers, as: Routes

   def mount(_params, _session, socket) do
      if connected?(socket), do: CategoryQuery.subscribe()
      {:ok, assign(socket, page_title: "ساخت مجموعه", changeset: %CategorySchema{} |> CategorySchema.changeset())}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("category_new.html", assigns)
   end

   def handle_event("validate", %{"category_schema" => category_info}, socket) do

      changeset = %CategorySchema{} 
      |> CategorySchema.changeset(category_info)
      |> Map.put(:action, :insert)

      {:noreply, assign(socket, changeset: changeset)}
   end

   def handle_event("save", %{"category_schema" => category_info}, socket) do
      case CategoryQuery.create_category(category_info) do
         {:ok, resutl} ->

            Erfan.Notif.notify_subscribers(:ok, "Category has been created", [:category, :new])

            {:noreply,
               socket
               # |> redirect(to: Routes.live_path(ErfanWeb.Endpoint, Categories))
               |> push_redirect(to: Routes.live_path(socket, Categories))
            }
         {:error, _, %Ecto.Changeset{} = changeset} ->
           {:noreply, assign(socket, changeset: changeset)}
       end
   end

   def handle_info({CategoryQuery, [:category, _], _}, socket) do
      {:noreply, socket}
   end
end