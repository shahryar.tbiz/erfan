defmodule ErfanWeb.Live.Foods do
   use Phoenix.LiveView

   alias Erfan.Admin.CategoryQuery
   alias Erfan.Admin.CategorySchema 

   alias ErfanWeb.Router.Helpers, as: Routes

   def mount(%{"id" => id}, _session, socket) do
      if connected?(socket), do: CategoryQuery.subscribe()
      {:ok, fetch(socket, id)}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("foods.html", assigns)
   end

   defp fetch(socket, id) do
      assign(socket, foods: CategoryQuery.show_category_foods(id) , page_title: "غذا ها", category_id: id)
   end
   

   def handle_event("delete_food", value, socket) do
      Erfan.Notif.notify_subscribers(:ok, "Food has been deleted", [:food, :delete])
      CategoryQuery.delete_food(value["food-id"])
      {:noreply, fetch(socket, value["food-categoryid"])}
   end

   def handle_info({CategoryQuery, [:food, :create_food], result}, socket) do
      {:noreply, fetch(socket, result.category_id)}
   end
   
   def handle_info({CategoryQuery, [:food, :edit_food], result}, socket) do
      {:noreply, fetch(socket, result.category_id)}
   end

   def handle_info({CategoryQuery, [:food, :delete_food], result}, socket) do
      {:noreply, fetch(socket, result.category_id)}
   end

   def handle_info({CategoryQuery, [:food, _], result} = params, socket) do
      {:noreply, fetch(socket, result.category_id)}
   end

   def handle_info({CategoryQuery, [:category, _], _result}, socket) do
      {:noreply, socket}
   end
end
