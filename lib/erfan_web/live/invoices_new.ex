defmodule ErfanWeb.Live.Invoices.New do
   use Phoenix.LiveView
   alias Erfan.Admin.CategoryQuery
   alias Erfan.Admin.InvoiceQuery


   def mount(_params, _session, socket) do
      {:ok, assign(socket, page_title: "ساخت فاکتور", categories: CategoryQuery.show_categories(), client_foods: [], foods: [], foodcollapse: "unshow", chair: 0, service: 0, total_amount: 0, error: false, status: 1)}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("invoices_new.html", assigns)
   end

   def handle_event("show-category-foods", %{"category-id" => category_id}, socket) do
      {:noreply, assign(socket, foods: CategoryQuery.show_category_foods(category_id), foodcollapse: "unshow")}
   end

   def handle_event("invoice_status", %{"status" => status}, socket) do
      IO.inspect status
      {:noreply, assign(socket, status: String.to_integer(status))}
   end

   def handle_event("service_price", %{"service-price" => service}, socket) do
      total_amount = sum_client_list_foods_amount(socket.assigns.client_foods)
      if service == "" do
         {:noreply, assign(socket, service: 0, total_amount: 0 + total_amount)}
      else
         {:noreply, assign(socket, service: String.to_integer(service), total_amount: String.to_integer(service) + total_amount)}
      end
   end

   def handle_event("delete-client-food-list", %{"foodid" => foodid}, socket) do
      client_foods = Enum.reject(socket.assigns.client_foods, fn x -> x.food_id == foodid end)
      {:noreply, assign(socket, client_foods: client_foods,
      total_amount: sum_client_list_foods_amount(client_foods), service: 0
      )}
   end

   def handle_event("select_chair", %{"chair-number" => chair_number}, socket) do
      {:noreply, assign(socket, chair: String.to_integer(chair_number))}
   end

   def handle_event("save_invoice", _value, socket) do
      with  {:ok, :check_client_foods, client_foods} <- check_client_foods(socket),
            {:ok, :check_chair, chair} <- check_chair(socket),
            {:ok, :check_total_amount, total_amount} <- check_total_amount(socket),
            {:ok, result} <- InvoiceQuery.create_invoice(%{
                  "title" => "فاکتور صندلی: #{chair} --شناسه: #{Erfan.randstring(6)}", 
                  "service" => socket.assigns.service,
                  "status" => socket.assigns.status,
                  "total_amount" => total_amount
               }) do

                  Erfan.Notif.notify_subscribers(:ok, "Invoice has been created", [:invoice, :new])
                  save_invoice_foods(result.id, client_foods)
                  reset_invoice(socket)
      else
         
         {:error, :check_client_foods} -> 

            {:noreply, assign(socket, error: true, ermsg: 
            %{
               سفارش: ["حداقل باید یک غذا در لیست سفارش شما باشد"]
             }
            )}
         
         
         {:error, :check_chair} -> 

            {:noreply, assign(socket, error: true, ermsg: 
            %{
               "شماره صندلی": ["لطفا صندلی را انتخاب کنید"]
             }
            )}


         {:error, :check_total_amount} -> 

            {:noreply, assign(socket, error: true, ermsg: 
            %{
               "قیمت کل": ["شما حتما باید محصولات خودتان را وارد نمایید تا قیمت کل محاسبه گردد"]
             }
            )}


         {:error, event, reason} -> 
            {:noreply, assign(socket, error: true, ermsg: Erfan.convert_changeset_errors(reason))}

         _ ->   
            reset_invoice(socket)
      end
   end

   
   def handle_event("save-client-food", value, socket) do
      {:ok, :get_food_by_id, food_info} = CategoryQuery.get_food_by_id(value["foodid"])
      new_client_food = %{
         food_id: food_info.id,
         title: food_info.title,
         price: food_info.price,
         number: value["number"]
      }
      total_amount = [new_client_food | socket.assigns.client_foods]

      {:noreply, assign(
                  socket, 
                  foods: CategoryQuery.show_category_foods(value["category_id"]), 
                  foodcollapse: "show",
                  client_foods: total_amount,
                  total_amount: sum_client_list_foods_amount(total_amount) + socket.assigns.service 
               )
      }
   end

   defp reset_invoice(socket) do
      {:noreply, assign(socket, page_title: "ساخت فاکتور", categories: CategoryQuery.show_categories(), client_foods: [], foods: [], foodcollapse: "unshow", chair: 0, service: 0, total_amount: 0, error: false, status: 1)}
   end


   defp sum_client_list_foods_amount(list_of_foods) do
      Enum.map(list_of_foods, fn x -> x.price * String.to_integer(x.number) end)
      |> Enum.sum()
   end

   defp check_client_foods(socket) do
      if socket.assigns.client_foods != [] do
         {:ok, :check_client_foods, socket.assigns.client_foods}
      else
         {:error, :check_client_foods}
      end
   end

   defp check_chair(socket) do
      if socket.assigns.chair != 0 do
         {:ok, :check_chair,socket.assigns.chair}
      else
         {:error, :check_chair}
      end
   end

   defp check_total_amount(socket) do
      if socket.assigns.total_amount > 0 do
         {:ok, :check_total_amount,socket.assigns.total_amount}
      else
         {:error, :check_total_amount}
      end
   end

   defp save_invoice_foods(invoice_id, client_foods) do
      Enum.map(client_foods, fn x -> 
         InvoiceQuery.create_invoice_food(%{
            invoice_id: invoice_id,
            number: x.number,
            food_id: x.food_id
         })
      end)
   end  
end
