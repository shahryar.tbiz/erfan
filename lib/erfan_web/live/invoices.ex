defmodule ErfanWeb.Live.Invoices do
   use Phoenix.LiveView
   alias Erfan.Admin.CategoryQuery
   alias Erfan.Admin.InvoiceQuery


   def mount(%{"page_number" => page_number}, _session, socket) do
      if connected?(socket), do: InvoiceQuery.subscribe()
      {:ok, fetch(socket, page_number)}
   end

   def mount(_params, _session, socket) do
      if connected?(socket), do: InvoiceQuery.subscribe()
      {:ok, fetch(socket, 1)}
   end

   def render(assigns) do
      ErfanWeb.PageView.render("invoices.html", assigns)
   end

   defp fetch(socket, pagenumber) do
      assign(socket, invoices: InvoiceQuery.show_invoices(pagenumber), page_title: "فاکتور ها", page_number: pagenumber)
   end

   def handle_event("delete_invoice", %{"invoice-id" => invoice_id}, socket) do
      InvoiceQuery.delete_invoice_and_invoice_foods(invoice_id)
      Erfan.Notif.notify_subscribers(:ok, "Invoice has been deleted", [:invoice, :deleted])
      {:noreply, fetch(socket, socket.assigns.page_number)}
   end

   def handle_event("show_invoice", %{"invoice-id" => invoice_id}, socket) do
      {:noreply, socket}
   end

   def handle_event("print_invoice", %{"invoice-id" => invoice_id}, socket) do
      IO.inspect "print_invoice"
      {:noreply, socket}
   end


   def handle_info({InvoiceQuery, [:invoice, :create_invoice], _}, socket) do
      {:noreply, fetch(socket, socket.assigns.page_number)}
   end
   
   def handle_info({InvoiceQuery, [:invoice, :edit_invoice], _}, socket) do
      {:noreply, fetch(socket, socket.assigns.page_number)}
   end

   def handle_info({InvoiceQuery, [:invoice, :delete_invoice], _}, socket) do
      {:noreply, fetch(socket, socket.assigns.page_number)}
   end

   def handle_info({InvoiceQuery, [:invoice, _], _}, socket) do
      {:noreply, fetch(socket, socket.assigns.page_number)}
   end
 end
 