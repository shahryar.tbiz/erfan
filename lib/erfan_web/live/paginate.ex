defmodule ErfanWeb.Live.Paginate do
  # use Phoenix.LiveComponent
  use Phoenix.LiveView
  alias ErfanWeb.Router.Helpers, as: Routes

  def mount(_params, %{"end_number" => end_number, "page_number" => page_number, "mod" => mod}, socket) do
    {:ok, assign(socket, page_number: page_number, end_number: end_number, mod: mod)}
  end

  def render(assigns) do
    ~L"""
    <div class="spacer30">  </div>
    <div class="container text-center mx-auto">
        <nav aria-label="Page navigation example" class="text-center col-md-6 mx-auto">
        <ul class="pagination mx-auto text-center col-md-6">
          <li class="page-item">
            <a phx-click="go_selected_page" phx-value-page-id="1" class="page-link invoice-link-color">
              اولین
            </a>
          </li>
          <%= for item <- navigation(@page_number, @end_number) do %>
            <li class="page-item">
              <%= if item == @page_number or Integer.to_string(item) == @page_number do %>
              <a phx-click="go_selected_page" phx-value-page-id="<%= item %>" class="page-link invoice-link-color-red">
                <%= item %>
              </a>
              <% else %>
                <a phx-click="go_selected_page" phx-value-page-id="<%= item %>" class="page-link invoice-link-color">
                  <%= item %>
                </a>
              <% end %>
              

            </li>
          <% end %>
          <li class="page-item">
            <a phx-click="go_selected_page" phx-value-page-id="<%= @end_number %>" class="page-link invoice-link-color">
              آخرین
            </a>
          </li>
        </ul>
        </nav>
    </div>
    """
  end


  def handle_event("go_selected_page", %{"page-id" => page_id}, socket) do
    {:noreply,
        socket
        |> push_redirect(to: Routes.live_path(socket, socket.assigns.mod, page_id))
    }
  end
  
  def navigation(requested_page_number, end_page_number) do
    start_number = compare_with_pagenumber(requested_page_number, end_page_number)
    start_number..start_number + 5
    |> Enum.to_list
    |> Enum.filter(fn(x) -> x <= end_page_number end)
  end

  def compare_with_pagenumber(requested_page_number, end_page_number) when requested_page_number <= end_page_number do
    requested_page_number
    |> integer_geter
  end

  def compare_with_pagenumber(_requested_page_number, _end_page_number), do: 1

  def integer_geter(string) do
    output = "#{string}"
    |> String.replace(~r/[^\d]/, "")

    if output == "", do: 1, else: String.to_integer(output)
  end

end
