defmodule ErfanWeb.PageController do
  use ErfanWeb, :controller
  import Phoenix.LiveView.Controller
  
  def index(conn, _params) do
    live_render(conn, ErfanWeb.Live.Index , session: %{})
  end

  def categories(conn, _params) do
    live_render(conn, ErfanWeb.Live.Categories , session: %{})
  end
end
