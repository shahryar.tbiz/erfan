defmodule Erfan.Admin.InvoiceFoodSchema do
   use Ecto.Schema

   import Ecto.Changeset
   @primary_key {:id, :binary_id, autogenerate: true}
   @foreign_key_type :binary_id
 
    schema "invoice_foods" do
 
     field :number, :integer, null: false
 
      belongs_to :invoices, Erfan.Admin.InvoiceSchema, foreign_key: :invoice_id, type: :binary_id
      belongs_to :foods, Erfan.Admin.FoodSchema, foreign_key: :food_id, type: :binary_id

     timestamps()
 
    end
 
      @all_fields ~w(number invoice_id food_id)a
      def changeset(struct, params \\ %{}) do
        struct
        |> cast(params, @all_fields)
        |> validate_required(@all_fields, message: "فیلد مذکور نمی تواند خالی باشد.")
        |> foreign_key_constraint(:invoice_id)
        |> foreign_key_constraint(:food_id)
      end
end
 