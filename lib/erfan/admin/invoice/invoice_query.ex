defmodule Erfan.Admin.InvoiceQuery do
   alias Erfan.Admin.InvoiceFoodSchema
   alias Erfan.Admin.InvoiceSchema

   import  Ecto.Query
   alias Erfan.Repo


   @topic inspect(__MODULE__)


   def subscribe do
      Phoenix.PubSub.subscribe(Erfan.PubSub, @topic)
   end


   defp notify_subscribers({:error, reason}, event) do
      {:error, event, reason}
    end
  
   defp notify_subscribers({:ok, result}, event) do
      Phoenix.PubSub.broadcast(Erfan.PubSub, @topic, {__MODULE__, event, result})
      Phoenix.PubSub.broadcast(Erfan.PubSub, @topic <> "#{result.id}", {__MODULE__, event, result})
      {:ok, result}
   end



   def get_invoice_by_id(id) do
      case Repo.get(InvoiceSchema, id) do
         nil -> {:error, :get_invoice_by_id}
         invoice -> {:ok, :get_invoice_by_id, invoice}
      end
   end

   def create_invoice(attrs) do
      %InvoiceSchema{}
      |> InvoiceSchema.changeset(attrs)
      |> Repo.insert()
      |> notify_subscribers([:invoice, :create_invoice])
   end



   def show_invoices(pagenumber) do
      query = from u in InvoiceSchema,
          order_by: [desc: u.inserted_at],
          select: %{
            id: u.id,
            title: u.title,
            service: u.service,
            status: u.status,
            total_amount: u.total_amount,
            
            inserted_at: u.inserted_at,
            updated_at: u.updated_at,
          }
      Repo.paginate(query, %{page: pagenumber, page_size: 20})
    end




    def update_invoice(current_info, attrs) do
      InvoiceSchema.changeset(current_info, attrs)
      |> Repo.update
    end

   def edit_invoice(id, attrs) do
      with {:ok, invoice_id} <- Ecto.UUID.cast(id),
           {:ok, :get_invoice_by_id, invoice_info} <- get_invoice_by_id(invoice_id),
           {:ok, resutl} = update <- update_invoice(invoice_info, attrs) do

            update
            |> notify_subscribers([:invoice, :edit_invoice])

            {:ok, resutl}
      else
         {:error, :get_invoice_by_id} ->
            notify_subscribers({:error, "Invoice dosent exist"}, [:invoice, :edit_invoice])
            {:error, :get_invoice_by_id, "_"}
        {:error, msg} ->
         notify_subscribers({:error, msg}, [:invoice, :edit_invoice])
         {:error, msg}
        _ ->
         notify_subscribers({:error, "Invoice dosent exist"}, [:invoice, :edit_invoice])
         {:error, :edit_invoice, "_"}
      end
   end



    def delete_invoice_and_invoice_foods(invoice_id) do
      delete_invoice_foods(invoice_id)
      delete_invoice(invoice_id)
    end


    def delete_invoice(invoice_id) do
      case get_invoice_by_id(invoice_id) do
         {:error, :get_invoice_by_id} -> {:error, :delete_invoice}

         {:ok, :get_invoice_by_id, invoice} ->
            Repo.delete(invoice)
            |> notify_subscribers([:invoice, :delete_invoice])
      end
    end


   def get_invoice_food_by_id(id) do
      case Repo.get(InvoiceFoodSchema, id) do
         nil -> {:error, :get_invoice_food_by_id}
         invoice_food -> {:ok, :get_invoice_food_by_id, invoice_food}
      end
   end

   def create_invoice_food(attrs) do
      %InvoiceFoodSchema{}
      |> InvoiceFoodSchema.changeset(attrs)
      |> Repo.insert()
      # |> notify_subscribers([:invoice_food, :create_invoice_food])
   end

   def delete_invoice_food(invoice_food) do
      Repo.delete(invoice_food)
   end

   def delete_invoice_foods(invoice_id) do
      show_invoice_foods_with_invoice_id(invoice_id, "delete")
      |> Enum.map(fn item -> 
         case get_invoice_food_by_id(item.id) do
            {:ok, :get_invoice_food_by_id, invoice_food} -> 
               delete_invoice_food(invoice_food)

            {:error, :get_invoice_food_by_id}  -> {:error, :get_invoice_food_by_id}
         end
      end)
    end


    def status_to_string(status_number) do
      case status_number do
         1 -> "پرداخت نشده"
         2 -> "پرداخت شده"
         3 -> "لغو شده"
      end
    end

    def show_invoice_foods_with_invoice_id(invoice_id, "delete") do
      query = from u in InvoiceFoodSchema,
          where: u.invoice_id == ^invoice_id,
          join: c in assoc(u, :foods),
          select: %{
            id: u.id,
            number: u.number,
            food_id: u.food_id,
            invoice_id: u.invoice_id,

            title: c.title,
            image: c.image,
            price: c.price,
            food_id: c.id,
          }
      Repo.all(query)
   end

   def show_invoice_by_id(invoice_id) do
      query = from u in InvoiceSchema,
          where: u.id == ^invoice_id,
         #  join: c in assoc(u, :invoice_foods),
          select: %{
            id: u.id,
            title: u.title,
            service: u.service,
            status: u.status,
            total_amount: u.total_amount,
            inserted_at: u.inserted_at,
            updated_at: u.updated_at,
          }
      Repo.one(query)
   end
end