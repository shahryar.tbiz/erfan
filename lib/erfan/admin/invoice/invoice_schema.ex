defmodule Erfan.Admin.InvoiceSchema do
   use Ecto.Schema

   import Ecto.Changeset
   @primary_key {:id, :binary_id, autogenerate: true}
   @foreign_key_type :binary_id
 
     schema "invoices" do
 
      field :title, :string, size: 150, null: false
      field :service, :integer, null: false
      field :status, :integer, null: false
      field :total_amount, :integer, null: true

      has_many :invoice_foods, Erfan.Admin.InvoiceFoodSchema ,  foreign_key: :invoice_id, on_delete: :nothing

      timestamps()
     end
 
     @all_fields ~w(title service status total_amount)a
     @required_fields ~w(status title service)a
 
     def changeset(struct, params \\ %{}) do
       struct
       |> cast(params, @all_fields)
       |> validate_required(@required_fields, message: "فیلد مذکور نمی تواند خالی باشد.")
       |> validate_length(:title, max: 150, message: "حداکثر ۱۵۰ کاراکتر")
       |> validate_inclusion(:status, [1, 2, 3], message: "وضعیت خرید شما باید عدیدی بین ۱ تا سه باشد")
     end
end