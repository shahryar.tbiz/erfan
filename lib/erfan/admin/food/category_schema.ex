defmodule Erfan.Admin.CategorySchema do
   use Ecto.Schema

   import Ecto.Changeset
   @primary_key {:id, :binary_id, autogenerate: true}
   @foreign_key_type :binary_id
 
     schema "categories" do
 
      field :title, :string, size: 150, null: false
      field :image, :string, size: 200, null: false

      has_many :foods, Erfan.Admin.FoodSchema ,  foreign_key: :category_id, on_delete: :nothing

      timestamps()
     end
 
     @all_fields ~w(title image)a
 
     def changeset(struct, params \\ %{}) do
       struct
       |> cast(params, @all_fields)
       |> validate_required(@all_fields, message: "فیلد مذکور نمی تواند خالی باشد.")
       |> validate_length(:title, max: 150, message: "حداکثر ۱۵۰ کاراکتر")
       |> validate_length(:image, max: 200, message: "حداکثر باید ۲۰۰ کاراکتر باشد.")
     end
end