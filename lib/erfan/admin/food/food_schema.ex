defmodule Erfan.Admin.FoodSchema do
   use Ecto.Schema

   import Ecto.Changeset
   @primary_key {:id, :binary_id, autogenerate: true}
   @foreign_key_type :binary_id
 
    schema "foods" do
 
     field :title, :string, size: 150, null: false
     field :image, :string, size: 200, null: false
     field :number, :integer, null: false
     field :price, :integer, null: false
 
     belongs_to :categories, Erfan.Admin.CategorySchema, foreign_key: :category_id, type: :binary_id

     has_many :invoice_foods, Erfan.Admin.InvoiceFoodSchema ,  foreign_key: :food_id, on_delete: :nothing

     timestamps()
 
    end
 
      @all_fields ~w(title image number price category_id)a
      def changeset(struct, params \\ %{}) do
        struct
        |> cast(params, @all_fields)
        |> validate_required(@all_fields, message: "فیلد مذکور نمی تواند خالی باشد.")
        |> validate_length(:title, max: 150, message: "حداکثر 150 کاراکتر")
        |> validate_length(:image, max: 200, message: "حداکثر 200 کاراکتر")
        |> foreign_key_constraint(:category_id)
      end
end
 