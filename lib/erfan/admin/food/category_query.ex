defmodule Erfan.Admin.CategoryQuery do

   import  Ecto.Query
   alias Erfan.Repo

   @topic inspect(__MODULE__)

   alias Erfan.Admin.CategorySchema 
   alias Erfan.Admin.FoodSchema 
   
   def subscribe do
      Phoenix.PubSub.subscribe(Erfan.PubSub, @topic)
   end

   defp notify_subscribers({:error, reason}, event) do
      {:error, event, reason}
    end
  
   defp notify_subscribers({:ok, result}, event) do
      Phoenix.PubSub.broadcast(Erfan.PubSub, @topic, {__MODULE__, event, result})
      Phoenix.PubSub.broadcast(Erfan.PubSub, @topic <> "#{result.id}", {__MODULE__, event, result})
      {:ok, result}
   end


   def get_category_by_id(id) do
      case Repo.get(CategorySchema, id) do
         nil -> {:error, :get_category_by_id}
         category_info -> {:ok, :get_category_by_id, category_info}
      end
   end


   def create_category(attrs) do
      %CategorySchema{}
      |> CategorySchema.changeset(attrs)
      |> Repo.insert()
      |> notify_subscribers([:category, :create_category])
   end





   def update_category(current_info, attrs) do
      CategorySchema.changeset(current_info, attrs)
      |> Repo.update
    end

   def edit_category(id, attrs) do
      with {:ok, category_id} <- Ecto.UUID.cast(id),
           {:ok, :get_category_by_id, category_info} <- get_category_by_id(category_id),
           {:ok, resutl} = update <- update_category(category_info, attrs) do

            update
            |> notify_subscribers([:category, :edit_category])

            {:ok, resutl}
      else
         {:error, :get_category_by_id} ->
            notify_subscribers({:error, "category dosent exist"}, [:category, :edit_category])
            {:error, :get_category_by_id, "_"}
        {:error, msg} ->
         notify_subscribers(msg, [:category, :edit_category])
         {:error, msg}
        _ ->
         notify_subscribers({:error, "category dosent exist"}, [:category, :edit_category])
         {:error, :get_category_by_id, "_"}
      end
   end









   def delete_category(id) do
      with {:ok, category_id} <- Ecto.UUID.cast(id),
           {:ok, :get_category_by_id, category_info} <- get_category_by_id(category_id),
           {:error, :show_foods_by_category_id} <- show_foods_by_category_id(category_id),
           {:ok, _struct} = delete_struct <- Repo.delete(category_info) do

         delete_struct
         |> notify_subscribers([:category, :delete_category])
      else
        {:error, reason} ->
          notify_subscribers({:error, reason}, [:category, :delete_category])

        {:ok, :show_foods_by_category_id, _categories} ->

          notify_subscribers({:error, "در این مجموعه چند مطلب وجود دارد برای حذف آن باید اول مطالب را حذف کنید."}, [:category, :show_foods_by_category_id])
        _ ->
         notify_subscribers({:error, "category dosent exist"}, [:category, :delete_category])
      end
   end




   def show_foods_by_category_id(category_id) do
      query = from u in FoodSchema,
          order_by: [desc: u.inserted_at],
          where: u.category_id == ^category_id,
          join: c in assoc(u, :categories),
          select: %{
            id: u.id,
            title: u.title,
            image: u.image,
            number: u.number,
            price: u.price,
            category_id: u.category_id,
            category_title: c.title,
            category_image: c.image
          }
      case Repo.all(query) do
        []         -> {:error, :show_foods_by_category_id}
        categories -> {:ok, :show_foods_by_category_id, categories}
      end
   end


   def show_categories do
      query = from u in CategorySchema,
          order_by: [desc: u.inserted_at],
          select: %{
            id: u.id,
            title: u.title,
            image: u.image,
            inserted_at: u.inserted_at,
            updated_at: u.updated_at
          }
      Repo.all(query)
    end
  









   #  Foods
   def get_food_by_id(id) do
      case Repo.get(FoodSchema, id) do
         nil -> {:error, :get_food_by_id}
         food_info -> {:ok, :get_food_by_id, food_info}
      end
   end


   def create_food(attrs) do
      %FoodSchema{}
      |> FoodSchema.changeset(attrs)
      |> Repo.insert()
      |> notify_subscribers([:food, :create_food])
   end





   def update_food(current_info, attrs) do
      FoodSchema.changeset(current_info, attrs)
      |> Repo.update
    end

   def edit_food(id, attrs) do
      with {:ok, food_id} <- Ecto.UUID.cast(id),
           {:ok, :get_food_by_id, food_info} <- get_food_by_id(food_id),
           {:ok, resutl} = update <- update_food(food_info, attrs) do

            update
            |> notify_subscribers([:food, :edit_food])

            {:ok, resutl}
      else
         {:error, :get_food_by_id} ->
            notify_subscribers({:error, "category dosent exist"}, [:food, :edit_food])
            {:error, :get_food_by_id, "_"}
        {:error, msg} ->
         notify_subscribers(msg, [:food, :edit_food])
         {:error, msg}
        _ ->
         notify_subscribers({:error, "food dosent exist"}, [:food, :edit_food])
         {:error, :get_food_by_id, "_"}
      end
   end





   def delete_food(id) do
      with {:ok, food_id} <- Ecto.UUID.cast(id),
           {:ok, :get_food_by_id, food_info} <- get_food_by_id(food_id),
           {:ok, _struct} = delete_struct <- Repo.delete(food_info) do

         delete_struct
         |> notify_subscribers([:food, :delete_food])
      else
        {:error, reason} ->
          notify_subscribers({:error, reason}, [:food, :delete_food])
        _ ->
         notify_subscribers({:error, "food dosent exist"}, [:food, :delete_food])
      end
   end

   def show_category_foods(category_id) do
      query = from u in FoodSchema,
          order_by: [desc: u.inserted_at],
          where: u.category_id == ^category_id,
          join: c in assoc(u, :categories),
          select: %{
            id: u.id,
            title: u.title,
            image: u.image,
            number: u.number,
            price: u.price,
            category_id: u.category_id,
            category_title: c.title,
            category_image: c.image
          }
          Repo.all(query)
   end

   def show_foods do
      query = from u in FoodSchema,
          order_by: [desc: u.inserted_at],
          select: %{
            id: u.id,
            title: u.title,
            image: u.image,
            inserted_at: u.inserted_at,
            updated_at: u.updated_at
          }
      Repo.all(query)
    end
  
end