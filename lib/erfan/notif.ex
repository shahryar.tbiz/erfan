defmodule Erfan.Notif do
   @topic inspect(__MODULE__)

   def subscribe do
      Phoenix.PubSub.subscribe(Erfan.PubSub, @topic)
   end

   def notify_subscribers(status, msg, event) do
      Phoenix.PubSub.broadcast(Erfan.PubSub, @topic, {__MODULE__, event, msg, status})
      {event, msg, status}
   end
end