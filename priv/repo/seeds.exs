# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Erfan.Repo.insert!(%Erfan.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
Erfan.Admin.CategoryQuery.create_category(%{image: "https://newerror.bankerror.com/images/icon-intro1-9176df04e4a18b21929a8f92961fabea.svg?vsn=d", title: "نوشیدنی سرد"})
Erfan.Admin.CategoryQuery.create_category(%{image: "https://newerror.bankerror.com/images/icon-intro1-9176df04e4a18b21929a8f92961fabea.svg?vsn=d", title: "نوشیدنی گرم"})
Erfan.Admin.CategoryQuery.create_category(%{image: "https://newerror.bankerror.com/images/icon-intro1-9176df04e4a18b21929a8f92961fabea.svg?vsn=d", title: "فست فود سرد"})
Erfan.Admin.CategoryQuery.create_category(%{image: "https://newerror.bankerror.com/images/icon-intro1-9176df04e4a18b21929a8f92961fabea.svg?vsn=d", title: "فست فود گرم"})
Erfan.Admin.CategoryQuery.create_category(%{image: "https://newerror.bankerror.com/images/icon-intro1-9176df04e4a18b21929a8f92961fabea.svg?vsn=d", title: "پیش غذا"})
