defmodule Erfan.Repo.Migrations.Foods do
  use Ecto.Migration
  @disable_ddl_transaction true

  def change do
    create table(:foods, primary_key: false) do
      add :id, :uuid, primary_key: true

      add :title, :string, size: 150, null: false
      add :number, :integer, null: false
      add :price, :integer, null: false

      add :image, :string, size: 200, null: false
      add :category_id, references(:categories, on_delete: :nothing, type: :uuid)

      timestamps()
    end
  end
end

