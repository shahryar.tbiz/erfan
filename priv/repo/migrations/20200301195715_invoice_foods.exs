defmodule Erfan.Repo.Migrations.InvoiceFoods do
  use Ecto.Migration
  @disable_ddl_transaction true

  def change do
    create table(:invoice_foods, primary_key: false) do
      add :id, :uuid, primary_key: true

      add :number, :integer, null: false

      add :invoice_id, references(:invoices, on_delete: :nothing, type: :uuid)
      add :food_id, references(:foods, on_delete: :nothing, type: :uuid)
      
      timestamps()
    end
  end
end