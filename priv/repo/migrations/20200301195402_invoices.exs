defmodule Erfan.Repo.Migrations.Invoices do
  use Ecto.Migration
  @disable_ddl_transaction true

  def change do
    create table(:invoices, primary_key: false) do
      add :id, :uuid, primary_key: true

      add :title, :string, size: 150, null: false
      add :service, :integer, null: false

      add :status, :integer, null: false


      add :total_amount, :integer, null: true

      timestamps()
    end
  end
end
