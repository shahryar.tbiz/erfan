defmodule Erfan.Repo.Migrations.Categories do
  use Ecto.Migration
    @disable_ddl_transaction true

    def change do
      create table(:categories, primary_key: false) do
        add :id, :uuid, primary_key: true

        add :title, :string, size: 150, null: false
        add :image, :string, size: 200, null: false
        timestamps()
      end
    end
end
