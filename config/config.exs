# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :erfan,
  ecto_repos: [Erfan.Repo]

# Configures the endpoint
config :erfan, ErfanWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "4mksuLztOgZMiDyV0G6LfRUVsHrFLAB5/Sg6Ztxjse493KFgvnsGv2J0i0J0hjms",
  render_errors: [view: ErfanWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Erfan.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "JRo4nWGu"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
